(function($) {
  Drupal.behaviors.html5LocalStorage = {
    attach: function(context, settings) {
      var isWindow = (context == window.document);
      // Whether browser support local storage
      var isStorage = (typeof(window['localStorage']) != undefined);
      // Whether browser support JSON
      var isJSON = (typeof(window['JSON']) != undefined);
      // Retrieve items for caching
      var cache = settings['HTML5LocalStorage'] && settings['HTML5LocalStorage']['cache'] || false;

      if (!isWindow || !isStorage || !isJSON || !cache) {
        return;
      }

      var expire = settings['HTML5LocalStorage']['expire'];
      var fieldStorage = new Drupal.behaviors.html5LocalStorage.Storage('html5LocalStorage', expire);
      fieldStorage.restore();

      var hierarchy = [
        'entityType',
        'bundle',
        'entityId',
        'fieldName',
        'ids'
      ];

      $.each(cache, function(index, fieldInfo) {
        var currentField = fieldStorage.getStorage();
        var currentLevel = null;
        /**
         * Build a sweet hierarchy.
         * Every level have:
         *  - values: Child objects
         *  - expire: expiration time
         *
         * Entity name:
         *  - Bundle name:
         *    - Entity id:
         *      - Field name:
         *       - HTML ids.
         */
        for (var i = 0, n = hierarchy.length; i < n; i++) {
          currentLevel = hierarchy[i];

          if (!currentField[ fieldInfo[currentLevel] ]) {
            if (currentLevel == 'ids') {
              currentField['ids'] = fieldInfo[currentLevel].slice();
            } else {
              currentField[ fieldInfo[currentLevel] ] = { value: {}, expire: (new Date).getTime() };
            }
          }

          if (currentLevel == 'ids') {
            for (var j = 0, s = currentField['ids'].length; j < s; j++) {
              var tempFieldName = currentField['ids'][j];
              if (!currentField[tempFieldName]) {
                currentField[tempFieldName] = { value: undefined, expire: (new Date).getTime() };
              }
            }
            currentField = currentField['ids'];
          } else {
            currentField = currentField[ fieldInfo[currentLevel] ].value;
          }
        }

        // Iterate through HTML ids of the field.
        for (i = 0, n = currentField.length; i < n; i++) (function(fieldId, fieldInfo) {
          // Clone object. Prevent passing obj by reference.
          var newFieldInfo = jQuery.extend(true, {'id': fieldId}, fieldInfo);

          fieldStorage.addField(newFieldInfo);
        })(currentField[i], fieldInfo);
      });

      fieldStorage.fill();
      fieldStorage.run(1000);
    }
  };
  
  /**
   * Constructor.
   *
   * @param {String} key
   *  localStorage identifier. (E.g window.localStorage(key, data) )
   *  @param {Number} expirationTime
   *
   * @constructor
   */
  function Storage(key, expirationTime) {
    this.storage = {};
    this.storageKey = key;
    this.fields = [];
    this.collectValues = $.proxy(this.collectValues, this);
    // Convert into milliseconds.
//    this._expire = +expirationTime * 3600000;
    this._expire = 30000;
  }

  Drupal.behaviors.html5LocalStorage.Storage = Storage;

  /**
   * Retrieve storage from the users localStorage
   *
   * @return {*}
   */
  Storage.prototype.restore = function() {
    var storage = localStorage.getItem(this.storageKey);
    if (!storage) {
      return {};
    }

    try {
      storage = JSON.parse(storage);
    } catch (e) {
      return {};
    }

    console.log(storage);
    this.storage = storage;

    return storage;
  };

  /**
   * Save Storage to the users localStorage
   *
   * @param storage
   */
  Storage.prototype.save = function(storage) {
    try {
      if (storage) {
        localStorage.setItem(this.storageKey, JSON.stringify(storage));
      } else {
        localStorage.setItem(this.storageKey, JSON.stringify(this.storage));
      }
    } catch (e) {
      if (e == QUOTA_EXCEEDED_ERR) {
        alert('Your local storage are full');
      }
    }
  };

  /**
   * Update field with given value.
   *
   * @param fieldInfo
   * @param value
   *  New value.
   *
   * @return {Boolean}
   *  Whether value is new or the same as existing.
   */
  Storage.prototype.update = function(fieldInfo, value) {
    var field = this.findField(fieldInfo);
    if (!field) return false;

    if (field[ fieldInfo['id'] ].value == value) {
      return false;
    }

    field[ fieldInfo['id'] ].value = value;
    field[ fieldInfo['id'] ].expire = (new Date()).getTime();

    return true;
  };

  /**
   * Return fieldName level by reference.
   * Thereby we can manipulate with field values outside.
   *
   * @param fieldInfo
   * @return {*}
   */
  Storage.prototype.findField = function(fieldInfo) {
    var bundle = null;
    var entityId = null;
    var fieldName = null;
    var entityType = null;
    var now = (new Date).getTime();

    if (!this.storage[fieldInfo.entityType]) return null;

    if (this.storage[fieldInfo.entityType].expire + this._expire <= now) {
      this.clear(fieldInfo.entityType);
      return null;
    }

    entityType = this.storage[fieldInfo.entityType].value;
    if (!entityType) return null;

    if (!entityType[fieldInfo.bundle]) return null;
    if (entityType[fieldInfo.bundle].expire + this._expire <= now) {
      this.clear(fieldInfo.entityType, fieldInfo.bundle);
      return null;
    }

    bundle = entityType[fieldInfo.bundle].value;
    if (!bundle) return null;

    if (!bundle[fieldInfo.entityId]) return null;
    if (bundle[fieldInfo.entityId] && bundle[fieldInfo.entityId].expire + this._expire <= now) {
      this.clear(fieldInfo.entityType, fieldInfo.bundle, fieldInfo.entityId);
      return null;
    }

    entityId = bundle[fieldInfo.entityId].value;
    if (!entityId) return null;

    if (!entityId[fieldInfo.fieldName]) return;
    if (entityId[fieldInfo.fieldName].expire + this._expire <= now) {
      this.clear(fieldInfo.entityType, fieldInfo.bundle, fieldInfo.entityId, fieldInfo.fieldName);
      return null;
    }

    fieldName = entityId[fieldInfo.fieldName].value;

    return fieldName;
  };

  /**
   * Return all storage object.
   *
   * @return {*}
   */
  Storage.prototype.getStorage = function() {
    return this.storage;
  };

  /**
   * Return field value from storage.
   *
   * @param fieldInfo
   * @return {*}
   */
  Storage.prototype.getFieldValue = function(fieldInfo) {
    var field = this.findField(fieldInfo);

    if (field) {
      return field[ fieldInfo['id'] ].value;
    }

    return undefined;
  };

  /**
   * Run collectValues in cycle with delay
   *
   * @see collectValues
   */
  Storage.prototype.run = function(delay) {
    setInterval(this.collectValues, delay);
  };

  /**
   * Add field to the storage.
   *
   * @param fieldInfo
   */
  Storage.prototype.addField = function(fieldInfo) {
    this.fields.push(fieldInfo);
  };

  /**
   * Fill default values.
   */
  Storage.prototype.fill = function() {
    var self = this;
    $.each(this.fields, function(index, fieldInfo) {
      var defaultValue = self.getFieldValue(fieldInfo);

      // Store data always when user change the field or type new data
      fieldInfo.field = $('#' + fieldInfo.id);

      if (defaultValue) {
        fieldInfo.field.val(defaultValue);
      }

      if (window['CKEDITOR'] && window['CKEDITOR'].instances[fieldInfo.id]) {
        window['CKEDITOR'].instances[fieldInfo.id].setData(defaultValue);
      }
    });
  };

  /**
   * Values collector.
   *
   * Iterates through fields, retrieve and save latest values.
   */
  Storage.prototype.collectValues = function() {
    var self = this;

    $.each(this.fields, function(index, fieldInfo) {
      var value = fieldInfo.field.val();

      // Get value from Ckeditor if exist.
      if (window['CKEDITOR'] && window['CKEDITOR'].instances[fieldInfo.id]) {
        value = window['CKEDITOR'].instances[fieldInfo.id].getData(value);
      }

      if (self.update(fieldInfo, value)) {
        self.save();
      }
    });
  };


  Storage.prototype.clear = function(entityType, bundle, entityId, fieldName) {
    var paramsLen = arguments.length;
    var condition = false;

    switch (paramsLen) {
      case 1:
         delete this.storage[entityType];
        break;

      case 2:
        condition = this.storage[entityType] && this.storage[entityType].value[bundle];
        if (condition) {
          delete this.storage[entityType].value[bundle];
        }
        break;

      case 3:
         condition =
            this.storage[entityType]
            && this.storage[entityType].value[bundle]
            && this.storage[entityType].value[bundle].value[entityId];

        if (condition) {
          delete this.storage[entityType].value[bundle].value[entityId];
        }
        break;

      case 4:
        condition =
           this.storage[entityType]
              && this.storage[entityType].value[bundle]
              && this.storage[entityType].value[bundle].value[entityId]
              && this.storage[entityType].value[bundle].value[entityId].value[fieldName];

        if (condition) {
          delete this.storage[entityType].value[bundle].value[entityId].value[fieldName];
        }
    }

    this.save();
  };

})(jQuery);