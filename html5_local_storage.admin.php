<?php

/**
 * Page callback for admin/config/administration/html5_local_storage
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function html5_local_storage_admin_page($form, &$form_state) {
  $form['expire'] = array(
    '#type'        => 'textfield',
    '#title'       => t('Expire'),
    '#description' => t('Expiration time in hours. Example: 15.5'),
    '#default_value' => variable_get('html5_local_storage_expire', 48),
    '#size' => 24,
  );

  return system_settings_form($form);
}